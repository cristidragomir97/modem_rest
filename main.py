import threading, serial, time,json
import uuid, datetime
from flask import Flask, abort, request, jsonify, g
from modem import modem
from database import database
from colorama import Fore, Back, Style
import winsound

from flask_httpauth import HTTPBasicAuth
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)


_modem = modem()
api = Flask(__name__)
auth = HTTPBasicAuth()

api.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'
batch_id = ""


### USER_API
@api.route('/api/batch/request')
@auth.login_required
def _request():
    user_id = str(g.user['user_id'])
    print(Back.RED + Fore.WHITE + "NEW REQUEST BY: ",user_id)
    winsound.PlaySound('sound.wav', winsound.SND_FILENAME)
    return jsonify({'200': 'request sent'})


@api.route('/api/batch/get')
@auth.login_required
def get_instance():
    user_id = str(g.user['user_id'])
    if 'RESERVED' in _modem.STATE:
        return jsonify({'error': 'machine_already_reserved'})
    elif 'TURNED_ON' in _modem.STATE:
        return jsonify({'error': 'machine_not_loaded'})
    elif 'LOADED' in _modem.STATE:
        number_list = modem.get_numbers(_modem)
        _response = []

        database.create_batch(db, _modem.BATCH_ID, user_id)
        for i in range(0,len(number_list)):
            if number_list[i] is not None:
                print(number_list[i])
                _number = str(number_list[i][1])
                _port = "COM" + str(number_list[i][0])

                _now = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()
                print(database.save_number(db, _number, _now, modem.get_provider(_modem), _port, _modem.BATCH_ID))

                number_object = {
                    'number': _number,
                    'generated_at': _now
                }
                _response.append(number_object)

        response = jsonify({'batch_id': _modem.BATCH_ID,'numbers':_response })
        return response
    else:
        return None


@api.route('/api/batch/unreserve/<_batch_id>/<used_for>')
@auth.login_required
def unreserve_batch(_batch_id, used_for):
    res = []
    user_id = str(g.user['user_id'])
    if database.verify_batch(db, str(_batch_id), str(user_id)):
        try:
            numbers = database.numbers_in_batch(db, _batch_id)
            print(numbers)
            if len(numbers) > 0:
                for number in numbers:
                    this_number = {}
                    number = number['number']
                    database.set_used(db, number, _batch_id, used_for)
                    this_number['batch_id'] = _batch_id
                    this_number['number'] = number
                    this_number['used_for'] = used_for
                    res.append(this_number)
                _modem.STATE = 'TURNED_ON'
            else:
                return jsonify({'403': 'batch_not_active'})

        except ValueError:
            return jsonify({'400': 'bad request'})
        except Exception as e:
            print(str(e))
    else:
        return jsonify({'403': 'batch_not_yours'})
    return jsonify(res)


@api.route('/api/batch/unreserve/<_batch_id>/<used_for>/<_number>')
@auth.login_required
def unreserve_number(_batch_id,used_for,_number):
    user_id = str(g.user['user_id'])
    if database.verify_batch(db, str(_batch_id), str(user_id)):
        try:
            numbers = database.numbers_in_batch(db, batch_id)
            if len(numbers) > 0:
                for number in numbers:
                    this_number = {}
                    database.set_used(db, number, _batch_id, used_for)
                    this_number['batch_id'] = _batch_id
                    this_number['number'] = numbers
                    this_number['used_for'] = used_for
                    return jsonify(this_number)

            else:
                return jsonify({'403': 'batch_not_active'})

        except ValueError:
            return jsonify({'400': 'bad request'})
    else:
        return jsonify({'403': 'batch_not_yours'})


@api.route('/api/sms/sync/<_batch_id>')
@auth.login_required
def sync_batch_sms(_batch_id):
    user_id = str(g.user['user_id'])
    ports = []

    if database.verify_batch(db,str(_batch_id),str(user_id)):

        numbers = modem.INSTANCE_PORTS
        sms = modem.get_all_sms(_modem)

        _sms = []

        try:
            for i in range(0, len(sms)):
                for j in range(0, len(sms[i][1])):
                    this_sms = {}
                    _port = 'COM' + str(sms[i][0])
                    _number = str(numbers[i][1])
                    _from = sms[i][1][j]['sender']
                    _at = str(sms[i][1][j]['date'])
                    _content = sms[i][1][j]['content']
                    database.save_sms(db, _batch_id, _number, _at, _from, _content)
        except Exception as e:
            print(e)

        return jsonify({'state':'SYNCED'}),200
    else:
        return jsonify({'401': 'unauthorised'}), 401


@api.route('/api/sms/get/<_batch_id>')
@auth.login_required
def get_batch_sms(_batch_id):
    user_id = str(g.user['user_id'])
    if database.verify_batch(db, str(_batch_id), str(user_id)):
        return database.get_sms_batch(db,_batch_id)
    else:
        return jsonify({'401': 'unauthorised'}), 401


@api.route('/api/sms/get/<_batch_id>/to/<nr>')
@auth.login_required
def get_batch_sms_nr(_batch_id,nr):
    user_id = str(g.user['user_id'])
    if database.verify_batch(db, str(_batch_id), str(user_id)):
        return database.get_sms_for(db,_batch_id,nr), 201
    else:
        return jsonify({'401': 'unauthorised'}), 401


@api.route('/api/sms/get/<_batch_id>/from/<_from>')
@auth.login_required
def get_batch_sms_nr_from(_batch_id,_from):
    user_id = str(g.user['user_id'])
    if database.verify_batch(db, str(_batch_id), str(user_id)):
        return database.get_sms_by(db, _batch_id, _from), 201
    else:
        return jsonify({'401': 'unauthorised'}), 401


@api.route('/api/token')
@auth.login_required
def get_auth_token():
    token = Serializer(api.config['SECRET_KEY'], expires_in=600).dumps({'id': g.user['id']})
    return jsonify({'token': token.decode('ascii'), 'duration': 600})

### ADMIN API


@api.route('/admin/batch/load/<provider>')
def init(provider):
    print(Fore.GREEN + ' * got init request')
    try:
        print(provider)
        modem.load(_modem,str(uuid.uuid4().hex),provider)
        return jsonify({'batch_id': _modem.BATCH_ID})

    except Exception as e:
        return jsonify({'400':str(e)}),400


@api.route('/admin/batch/state')
def state():
    return jsonify({'state': _modem.STATE}), 200


@api.route('/admin/batch/cancel')
def cancel():
    if 'RESERVED' in _modem.STATE:
        _modem.STATE = 'LOADED'
        return jsonify({'state':'LOADED'}),200
    if 'LOADED' in _modem.STATE:
        _modem.STATE = 'TURNED_ON'
        return jsonify({'state': 'TURNED_ON'}), 200
    if 'TURNED_ON' in _modem.STATE:
        return jsonify({'state': 'TURNED_ON'}), 200
    else:
        return jsonify({'400':'bad_request'}),400


@api.route('/admin/batch/close/<id>')
def close(id):
    database.close_batch(db, id)
    return jsonify({'closed': id}), 200


@api.route('/admin/all/numbers')
def table_numbers():
    return database.table_numbers(db)


@api.route('/admin/all/batches')
def table_batches():
    return database.table_batch(db)


@api.route('/admin/all/messages')
def table_messages():
    return database.table_sms(db)


@api.route('/admin/all/used')
def table_used():
    return database.table_used(db)

@api.route('/admin/all/users')
def table_users():
    return database.table_users(db)


@api.route('/admin/numbers/<number>')
def describe_number(number):
    return database.describe_number(db,str(number))


@api.route('/admin/users/add', methods=['POST'])
def new_user():
    username = request.json.get('username')
    password = request.json.get('password')
    hashed_passwd = pwd_context.encrypt(password)
    added_at =  datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()
    user_id = uuid.uuid4().hex

    print(username, password)
    if username is None or password is None:
        abort(400)    # missing arguments
    if database.user_exists(db, username) is True:
        abort(400)    # existing user

    database.create_user(db,username, hashed_passwd, added_at, user_id)
    return jsonify({'username': username, "user_id": user_id, "added_at": added_at}), 201


@api.route('/admin/users/describe/<id>')
def get_user(id):
    user = database.user_by_id(db,id)
    if not user:
        abort(400)
    return jsonify({'username': user["username"],
                    "user_id": user["user_id"],
                    "added_at": user["added_at"]}), 201


# Auth methods
def verify_auth_token(token):
    s = Serializer(api.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    user = database.user_by_id(db, data['id'])
    return user

@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = verify_auth_token(username_or_token)
    print(user)
    if not user:
        # try to authenticate with username/password
        user = database.retrieve_user(db, username_or_token)
        if not user or not pwd_context.verify(password, user["passwd_hash"]):
            return False
    g.user = user
    return True




if __name__ == '__main__':
    db = database('newww.db')
    database.create_tables(db)
    print(Fore.GREEN + ' * db initialised')
    api.run(host="192.168.192.249")






















import time, serial, re, datetime
import smspdu.easy as decoder
import serial.tools.list_ports
from multiprocessing import Pool
import threading



# STATIC
def send_command(command, ser, timeout):
    command = command + "\r\n"
    ser.write(command.encode())
    time.sleep(timeout)
    read_me = ser.read(1024).decode('unicode_escape')
    return read_me

def init_port(PORT, verbose=False):
    ser = serial.Serial("COM" + str(PORT), 115200, timeout=1)

    # Echo disabled
    response = send_command("ATE0", ser, 0)
    if verbose: print("[-]COM{}@set_no_echo: {}".format(PORT, response))

    # Get manufacturer info
    response = send_command("AT+GMI", ser, 0)
    if verbose: print("[-]COM{}@get_manufacturer_info: {}".format(PORT, response))

    # Set text mode
    response = send_command("AT+GMM", ser, 0)
    response += send_command("AT+GSN", ser, 0)
    response += send_command("AT+CMGF=?", ser, 0)
    if verbose: print("[-]COM{}@set_text_mode: {}".format(PORT, response))

    # Set encoding to ISO-8859-1
    response = send_command("AT+CSCS=?", ser, 0)
    response += send_command("AT+GCAP", ser, 0)
    response += send_command("AT+CSCS=\"8859-1\"", ser, 0)
    if verbose: print("[-]COM{}@set_iso_mode: {}".format(PORT, response))

    # Store messages to SIM card
    response = send_command("AT+CMGF=0", ser, 0)
    response += send_command("AT+CPMS=?", ser, 0)
    response += send_command("AT + CPMS = \"SM\"", ser, 0)
    if verbose: print("[-]COM{}@set_storage_sim: {}".format(PORT, response))

    ser.close()

    '''
        if "EE" or "ee" in provider:
            # init ports
            pool = Pool(processes=_processes)
            pool.map(init_port, range(starting_port(), ports))
            pool.close()
            pool.join()

            # send sms
            pool = Pool(processes=_processes)
            pool.map(get_number_sms_send, range(starting_port, (starting_port + n_ports)))
            pool.close()
            pool.join()

            # sleep
            time.sleep(15)

            # recieve sms
            pool = Pool(processes=_processes)
            numberz = pool.map(get_number_sms_recieve, range(starting_port, (starting_port + n_ports)))
            pool.close()
            pool.join()

        elif "lebara" in provider:
            pool = Pool(processes=_processes)
            numberz = pool.map(get_number_ussd, range(starting_port, (starting_port + n_ports)))
            pool.close()
            pool.join()

    '''

def get_number_cnum(port, verbose=False):
    try:
        ser = serial.Serial("COM" + str(port), 115200, timeout=1)
        response = send_command("AT+CNUM", ser, 0)
        ser.close()

        phonenumber = re.search("\d{11}", response).group(0)
        if verbose: print("[-]COM{}@get_number_cnum: {}".format(port, phonenumber))
        return [port, phonenumber]

    except Exception as e:
        print("[-] Error: {}".format(e))
        return None

def get_number_ussd(port, ussd_code, verbose=False):
    try:
        ser = serial.Serial("COM" + str(port), 115200, timeout=1)
        send_command("AT+CSCS=\"GSM\"", ser, 0)
        response = send_command("AT+CUSD=1,\"" + str(ussd_code) + "\",15", ser, 4)
        ser.close()

        phonenumber = re.search("\d{11}", response).group(0)
        if verbose: print("[-]COM{}@get_number_ussd_{}: {}".format(port, ussd_code, phonenumber))
        return [port, phonenumber]

    except Exception as e:
        print("[-] COM{}:Error@get_number".format(port))
        return [port, e]

def get_number_sms_send(port, number, verbose=False):
    try:
        ser = serial.Serial("COM" + str(port), 115200, timeout=1)
        text = "NUMBER"
        send_command("ATE", ser, 0)
        send_command("AT+CMGF=1", ser, 0)
        response = send_command("AT+CMGW=" + "\"" + str(number) + "\"", ser, 1)

        if ">" in response and verbose:
            print("[-]COM{}@get_number_sms/got_prompt".format(port))

        sendTextMessage = send_command("NUMBER" + chr(26), ser, 1)

        if "+CMGW:" in sendTextMessage:
            index = re.findall("\d+", sendTextMessage)
            actuallySendSms = send_command("AT+CMSS=" + index[0], ser, 5)
            if verbose: print("[-]COM{}@get_number_sms/sent_sms {}".format(port, actuallySendSms))

        ser.close()

    except Exception as e:
        print(" > " + str(port) + " > " + "ERROR >" + str(e))

def get_sms(port, verbose=True):

    sms_list = []
    ser = serial.Serial("COM" + str(port), 115200, timeout=0)

    response = send_command("AT+CPMS=\"SM\"", ser, 1) + \
               send_command("AT+CMGL=1", ser, 1) + \
               send_command("AT+CMGL=0", ser, 1)


    ser.close()

    try:
        pduList = re.findall('\w{9,}', response)
        pduList = pduList[::-1]

        for pdu in pduList:
            sms_list.append(decoder.easy_sms(pdu))

        return [port, sms_list]
    except Exception as e:
        print(e)
        return


def get_number_sms_recieve(port, verbose=False):
    port, sms_list = get_sms(port, verbose)

    for i in range(0, len(sms_list) - 1):

        # print("SUB OUT:" + str(out))
        phone_numbers = re.findall('\d{11}', sms_list[0])
        phone_numbers = phone_numbers[::-1]

        if len(phone_numbers) > 0:
            string_phonenumner = str(phone_numbers[0])
        if "44795396625" in string_phonenumner:
            string_phonenumner = str(phone_numbers[1])
        return [port, string_phonenumner]


class modem:
    STATE = ''
    PROVIDER = ""
    PORTS = []
    COMS = []

    starting_port = ''
    n_ports = ''

    INSTANCE_PORTS = []
    INSTANCE_COMS = []

    BATCH_ID = ""

    def __init__(self):
        self.STATE = 'TURNED_ON'

    def load(self,batch_id, provider):
        numberz = []
        ports = []

        if 'TURNED_ON' in self.STATE:
            for port in serial.tools.list_ports.comports():
                port_name = re.search("(COM)\d+", port.description)[0]
                port_number = int(re.search("\d+", port_name)[0])
                if port_number > 1:
                    ports.append(port_number)

            starting_port = int(min(ports))
            n_ports = max(ports) - min(ports) + starting_port
            _processes = n_ports - starting_port
            _processes = int(_processes)

            self.starting_port = starting_port
            self.n_ports = n_ports

            if "three" in provider:
                print("CNUM FFS")
                pool = Pool(processes=_processes)
                numberz = pool.map(get_number_cnum, range(starting_port, n_ports + 1))
                pool.close()
                pool.join()
            else:
                raise ValueError

            for element in numberz:
                if element is not None:
                    _number = element
                    self.INSTANCE_PORTS.append(_number)
                    self.INSTANCE_COMS.append(_number[0])

            pool = Pool(processes=64)
            pool.map(init_port, range(starting_port, n_ports + 1))
            pool.close()
            pool.join()

            self.PROVIDER = provider
            self.STATE = "LOADED"
            self.BATCH_ID = batch_id


    def get_numbers(self):
        self.STATE = "RESERVED"

        return self.INSTANCE_PORTS

    def get_provider(self):
        return self.PROVIDER


    def remove_number(self,number):
        if len(self.INSTANCE_PORTS) > 0:
            self.INSTANCE_PORTS.remove(number)
        else:
            self.STATE = "TURNED_ON"

    def get_all_sms(self):
        print(str(self.INSTANCE_COMS))

        ''''
      
        threads = [threading.Thread(target=get_sms, args=(port,)) for port in self.INSTANCE_COMS]

        for thread in threads:
            # Starts all of them
            thread.start()
            thread.
        for thread in threads:
            # Joins them together so they exit at the same time
            thread.join()

        return threads
          '''
        pool = Pool(processes=64)
        sms = pool.map(get_sms, self.INSTANCE_COMS)
        pool.close()
        pool.join()

        sms = list(filter(None.__ne__, sms))

        return sms

    def get_messages(self, port):
        return get_sms(port)












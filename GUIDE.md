# swoosh.church/genmoji
___
##  Retrieve API Token 
 ```sh
curl -u USER:PASS -i -X GET https://swoosh.church/api/token
 ```
 ```json
 {
    "duration":600,
    "token":"USER_TOKEN"
}
 ```
 ___
 
##  Ring the bell  
 ```sh
curl -u USER_TOKEN:x -i -X GET https://swoosh.church/api/batch/request
 ``` 
``` json
{"200":"request sent"}
``` 
 ___

## Select current batch of sims

```sh
curl -u USER_TOKEN:x -i -X GET https://swoosh.church/api/batch/get
 ```
```json
{
    "batch_id": "217ffc0e93f24980b05520a1f57d1d2b",
    "numbers": [
        {
            "generated_at": "2018-08-24T15:58:08.689170+00:00",
            "number": "44738010176"
        },
      . . .
      ]
}

```
___
## Retrieve Messages
### Sync sms 
 ```sh
curl -u USER_TOKEN:x -i -X GET https://swoosh.church/api/sms/sync/BATCH_ID
  ```
 ```json
 {"state":"SYNCED"}
```

### Retrieve SMS for entire batch
```sh
curl -u  USER_TOKEN:x -i -X GET https://swoosh.church/api/sms/get/BATCH_ID
```
```json
[  
   {  
      "id":35,
      "phone_number":"44740325092",
      "recieved_at":"2018-08-25 21:25:34+00:00",
      "_from":"Rate Advice",
      "content":"ing info free call +447782333330",
      "batchid":"f9fc76b6f31a4af1880e81675486e701"
   },
   
   . . .
   
   {  
      "id":68,
      "phone_number":"44740325092",
      "recieved_at":"2018-08-20 08:27:34+00:00",
      "_from":"Rate Advice",
      "content":"ing info free call +447782333330",
      "batchid":"f9fc76b6f31a4af1880e81675486e701"
   }
]
```
### Retrieve SMS for specific number
```sh
curl -u  USER_TOKEN:x -i -X GET https://swoosh.church/api/sms/get/BATCH_ID/to/TO
```
```json
[  
   {  
      "id":54,
      "phone_number":"44740325092",
      "recieved_at":"2018-08-25 21:25:34+00:00",
      "_from":"Rate Advice",
      "content":"ing info free call +447782333330",
      "batchid":"f9fc76b6f31a4af1880e81675486e701"
   },
   {  
      "id":55,
      "phone_number":"44740325092",
      "recieved_at":"2018-08-20 08:27:34+00:00",
      "_from":"Rate Advice",
      "content":"ing info free call +447782333330",
      "batchid":"f9fc76b6f31a4af1880e81675486e701"
   }
]
```
### Retrieve SMS from specific sender
```sh
curl -u USER_TOKEN:x -i -X GET https://swoosh.church/api/sms/get/BATCH_ID/from/FROM
```
```json
[  
   {  
      "id":53,
      "phone_number":"44784621919",
      "recieved_at":"2018-08-20 18:07:00+00:00",
      "_from":"+447707888048",
      "content":"Sun 26th Cococure presents AfrobeatsBrunch hosted by Estare & Dj Sean. 1hr Bottomless cocktails & All you can eat by Enish Restaurant. Tkts- bit.ly/cccabrunch",
      "batch_id":"f9fc76b6f31a4af1880e81675486e701"
   }
]
```
___

##  Close Batch 

```sh
curl -u USER_TOKEN:x -i -X GET https://swoosh.church/api/batch/close/BATCH_ID
```
```json
[  
   {  
      "batch_id":"f9fc76b6f31a4af1880e81675486e701",
      "number":"44782828078",
      "used_for":"Nike"
   },
   . . . 
   {  
      "batch_id":"f9fc76b6f31a4af1880e81675486e701",
      "number":"44738311189",
      "used_for":"Nike"
   }
]

```
___
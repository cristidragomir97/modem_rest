import datetime, json, re
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, DateTime, MetaData, ForeignKey

URL = 'sqlite:///{}'

class database:
    db_engine = None
    
    def __init__(self, dbname=''):
        self.db_engine = create_engine(URL.format(dbname))

    def create_tables(self):
        print("DOOOH")
        metadata = MetaData()
        
        numbers = Table('numbers', metadata,
            Column('id', Integer, primary_key=True),
            Column('number', String, nullable=True),
            Column('generated_at',String, nullable=True),
            Column('provider',String, nullable=False),
            Column('port',String,nullable=True),
            Column('batch_id',String, ForeignKey('batches.batch_id')))

        messages = Table('messages', metadata,
            Column('id', Integer, primary_key=True),
            Column('phone_number', String, ForeignKey('numbers.number')),
            Column('recieved_at',String,nullable=False),
            Column('_from', String, nullable=False),
            Column('content', String,nullable=False),
            Column('batch_id',String,ForeignKey('batches.batch_id')))

            
        used = Table('used', metadata,
            Column('id', Integer, primary_key=True),
            Column('phone_number', None, ForeignKey('numbers.number'),nullable=False),
            Column('batch_id',String, nullable=False),
            Column('used_at', String, nullable=False),
            Column('used_for', String, nullable=False))

        batches =  Table('batches', metadata,
              Column('id', Integer, primary_key=True),
              Column('batch_id', String),
              Column('user_id', String),
              Column('created_at', String),
              Column('closed_at', String))

        users = Table('users',metadata,
              Column('id', Integer, primary_key=True),
              Column('username', String),
              Column('passwd_hash', String),
              Column('user_id', String),
              Column('added_at', String))



        try:
            metadata.create_all(self.db_engine)
        except Exception as e:
            print("Error occurred during Table creation!")
            print(e)

    def execute_raw_query(self,_query,debug=True):
        if _query == "": return
        if debug: print(_query)

        with self.db_engine.connect() as connection:
            try:
                connection.execute(_query)
            except Exception as e:
                print(e)


    ## USERS
    '''users = Table('users', metadata,
                  Column('id', Integer, primary_key=True),
                  Column('username', String),
                  Column('passwd_hash', String),
                  Column('user_id', String),
                  Column('added_at', String))
    '''
    def create_user(self,username,hashed_passwd,added_at,user_id):
        try:
            query = "INSERT INTO users (username,passwd_hash,user_id,added_at) \
                        VALUES ('{USRN}','{PWD}','{USERID}','{CREATED_AT}');".format(
                USRN =username,
                PWD=hashed_passwd,
                USERID=user_id,
                CREATED_AT=added_at
            )

            self.execute_raw_query(query)
        except Exception as e:
            print(e)

    def user_exists(self, username):
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM users WHERE \
                               username IS '{}'".format(username)
                result = connection.execute(query)
                if result is not None:
                    for line in result:
                        print(line)
                        if line:
                            return True

                return False
        except Exception as e:
            print(e)
            return False



    def user_by_id(self, id):
        user = {}
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM users where ID is '{}' LIMIT 1;".format(id)
                result = connection.execute(query)
                for line in result:
                    line = list(line)

                    _user_object = {
                        'id': line[0],
                        'username': line[1],
                        'passwd_hash': line[2],
                        'user_id': line[3],
                        'created_at': line[4]
                    }
                    user = _user_object
            return user
        except Exception as e:

            print('[x] {}'.format(e))
            return {}



    def retrieve_user(self, username):
        user = {}
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM users WHERE username is '{}' LIMIT 1;".format(username)
                result = connection.execute(query)
                for line in result:
                    line = list(line)

                    _user_object = {
                        'id': line[0],
                        'username': line[1],
                        'passwd_hash': line[2],
                        'user_id': line[3],
                        'created_at': line[4]
                    }
                    user = _user_object
            return user
        except Exception as e:

            print('[x] {}'.format(e))
            return {}

    def table_users(self):
        users = []
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM users;".format(id)
                result = connection.execute(query)
                for line in result:
                    line = list(line)

                    _user_object = {
                        'id': line[0],
                        'username': line[1],
                        'passwd_hash': line[2],
                        'user_id': line[3],
                        'created_at': line[4]
                    }
                    users.append(_user_object)

                return json.dumps(users)
        except Exception as e:
            print('[x] {}'.format(e))
            return json.dumps({'error': str(e)})

    ## BATCHES
    def create_batch(self,batch_id,user_id):
        try:
            query = "INSERT INTO batches (batch_id,user_id,created_at) \
                  VALUES ('{BATCHID}','{USERID}','{CREATED_AT}');".format(
                BATCHID = batch_id,
                USERID = user_id,
                CREATED_AT = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()
            )

            self.execute_raw_query(query)
        except Exception as e:
            print(e)

    def verify_batch(self,batch_id,user_id):
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM batches WHERE \
                         batch_id IS '{}' AND user_id IS '{}'".format(batch_id,user_id)
                result = connection.execute(query)
                print(query)
                if result is not None:
                    for line in result:
                        print(line)
                        if(line):
                            return True

                return False
        except Exception as e:
            print(e)
            return False




    def close_batch(self,batch_id):
        try:
                query = "UPDATE batches SET closed_at='{CLOSED_AT}' \
                            WHERE batch_id='{BATCHID}'".format(
                    BATCHID=batch_id,
                    CLOSED_AT=datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()
                )

                self.execute_raw_query(query)
        except Exception as e:
            print(e)




    def table_batch(self):
        _res = []
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM batches;".format()
                result = connection.execute(query)
                for line in result:
                    line = list(line)

                    _numbers_object = {
                        'id': line[0],
                        'batch_id': line[1],
                        'user_id': line[2],
                        'created_at': line[3],
                        'used_at:':line[4]
                    }
                    _res.append(_numbers_object)

            return json.dumps(_res)
        except Exception as e:
            print('[x] {}'.format(e))
            return json.dumps({'error': str(e)})

    ## NUMBERS 
    def save_number(self,_nr,_at,_provider,_port,_batch_id):
        print("DOOOH")
        try:
            query = "INSERT INTO numbers (number,generated_at,provider,port,batch_id) \
            VALUES ({NR},'{AT}','{PRO}','{PORT}','{BATCHID}')".format(NR=_nr,
            AT=_at,PRO=_provider,PORT=_port,BATCHID=_batch_id)

            self.execute_raw_query(query)
            print(query)
        except Exception as e:
            print(e)

    def describe_number(self,nr):
        _res = []
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM numbers WHERE number IS {};".format(nr)
                result = connection.execute(query)
                for line in result:
                    line = list(line)

                    _numbers_object = {
                        'id': line[0],
                        'number': line[1],
                        'generated_at': line[2],
                        'provider': line[3],
                        'port': line[4],
                        'batch_id': line[5]
                    }

                    _res.append(_numbers_object)

            return json.dumps(_res)
        except Exception as e:
            print('[x] {}'.format(e))
            return json.dumps({'error':str(e)})

    def table_numbers(self):
        _res = []
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM numbers ;"
                result = connection.execute(query)
                for line in result:
                    line = list(line)
                    print(line)

                    _numbers_object = {
                        'id': line[0],
                        'number': line[1],
                        'generated_at': line[2],
                        'provider': line[3],
                        'port': line[4],
                        'batch_id': line[5]
                    }

                    _res.append(_numbers_object)

            return json.dumps(_res)
        except Exception as e:
            print('[x] {}'.format(e))
            return json.dumps({'error':str(e)})

    def numbers_in_batch(self,batch_id):
        _res = []
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM numbers WHERE batch_id IS '{}';".format(batch_id)
                result = connection.execute(query)
                for line in result:
                    line = list(line)
                    print(line)

                    _numbers_object = {
                        'id': line[0],
                        'number': line[1],
                        'generated_at': line[2],
                        'provider': line[3],
                        'port': line[4],
                        'batch_id': line[5]
                    }

                    _res.append(_numbers_object)

            return _res
        except Exception as e:
            print('[x] {}'.format(e))
            return json.dumps({'error': str(e)})

    ## MESSAGES 
    def save_sms(self,batchid,nr,_now,_from,content):
        try:

            #code = re.findall("\d{6}\s",str(content))

            query = "INSERT INTO messages (phone_number,recieved_at,_from,content,batch_id) \
            VALUES ('{NR}','{NOW}','{FROM}','{CONTENT}','{BATCHID}')".format(NR=nr,
            NOW=_now,FROM=_from,CONTENT=str(content),BATCHID=batchid)

            self.execute_raw_query(query)
        except Exception as e:
            print(e)

    def get_sms_batch(self,batchid):
        _res = []
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM messages WHERE batch_id LIKE '{BATCHID}';".format(BATCHID=batchid)
                result = connection.execute(query)

                for line in result:
                    line = list(line)

                    _messages_object = {
                        'id': line[0],
                        'phone_number': line[1],
                        'recieved_at': line[2],
                        '_from': line[3],
                        'content': line[4],
                        'batchid': line[5]
                    }

                    _res.append(_messages_object)

            return json.dumps(_res)
        except Exception as e:
            print('[x] {}'.format(e))
            return json.dumps({'error':str(e)})

    def get_sms_for(self,batchid,nr):
        _res = []
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM messages WHERE phone_number LIKE '{NR}' \
                        AND batch_id LIKE '{BATCHID}'".format(NR=nr,BATCHID=batchid)
                result = connection.execute(query)

                for line in result:
                    line = list(line)

                    _messages_object = {
                        'id': line[0],
                        'phone_number': line[1],
                        'recieved_at': line[2],
                        '_from': line[3],
                        'content': line[4],
                        'batchid': line[5]
                    }

                    _res.append(_messages_object)

            return json.dumps(_res)
        except Exception as e:
            print('[x] {}'.format(e))
            return json.dumps({'error':str(e)})

    def get_sms_by(self,batchid,_from):
        _res = []
        try:
            with self.db_engine.connect() as connection:
                
                query = "SELECT * FROM messages WHERE _from LIKE '{FROM}' AND batch_id LIKE '{BATCHID}';".format(FROM=_from,BATCHID=batchid)
                result = connection.execute(query)

                for line in result:
                    line = list(line)

                    _messages_object = {
                        'id': line[0],
                        'phone_number': line[1],
                        'recieved_at': line[2],
                        '_from': line[3],
                        'content': line[4],
                        'batch_id': line[5]
                    }

                    _res.append(_messages_object)

            return json.dumps(_res)
        except Exception as e:
            print('[x] {}'.format(e))
            return json.dumps({'error':str(e)})

    def table_sms(self):
        _res = []
        try:
            with self.db_engine.connect() as connection:

                query = "SELECT * FROM messages"
                result = connection.execute(query)

                for line in result:
                    line = list(line)

                    _messages_object = {
                        'id': line[0],
                        'phone_number': line[1],
                        'recieved_at': line[2],
                        '_from': line[3],
                        'content': line[4],
                        'batch_id': line[5]
                    }

                    _res.append(_messages_object)

            return json.dumps(_res)
        except Exception as e:
            print('[x] {}'.format(e))
            return json.dumps({'error': str(e)})

    ## USED
    def set_used(self,nr,batch,_for):
        try:
            _now = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()
            query = "INSERT INTO used (phone_number,batch_id,used_at,used_for) \
            VALUES ({NR},'{BATCH}','{AT}','{FOR}')".format(NR=nr,BATCH=batch,AT=_now,FOR=_for)

            self.execute_raw_query(query)
        except Exception as e:
            print(e)

    def is_used(self,nr):
        _res = []
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM used WHERE phone_number IS {}".format(nr)
                result = connection.execute(query)

                for line in result:
                    line = list(line)

                    _line_obj = {
                        '_id': line[0],
                        'phone_number': line[1],
                        'batch_id': line[2],
                        'used_at': line[3],
                        'used_for': line[4]
                    }

                    _res.append(_line_obj)

            return json.dumps(_res)
        except Exception as e:
            print('[x] {}'.format(e))

    def table_used(self):
        _res = []
        try:
            with self.db_engine.connect() as connection:
                query = "SELECT * FROM used"
                result = connection.execute(query)

                for line in result:
                    line = list(line)

                    _line_obj = {
                        '_id': line[0],
                        'phone_number': line[1],
                        'batch_id': line[2],
                        'used_at': line[3],
                        'used_for': line[4]
                    }

                    _res.append(_line_obj)

            return json.dumps(_res)
        except Exception as e:
            print('[x] {}'.format(e))


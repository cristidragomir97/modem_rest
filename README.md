# Internal API 

## User Methods
### Batch 
#### GET /api/batch/request/<user_id>
##### Effect 
* notifies admin to physically change sims 
* lights a bulb or some shit
##### Returns 
* 403 Forbidden
 ```json
{“error” :”machine_already_reserved”}
```
* 200 Success
 ```json
{"200":"request_sent"}
```
___

#### GET /api/batch/get/<user_id>
##### Effect 
* Initialises current batch 
* Loads phone numbers from modem
* Returns list of numbers in the batch
##### Returns 
* 401 Unauthorised
 ```json
{“error” :”batch_not_yours”} 
```
* 403 Forbidden
 ```json
{“error” :”machine_already_reserved”}
{“error” :”machine_not_loaded”}
```

* 201 Created
```json
{
    "batch_id": "217ffc0e93f24980b05520a1f57d1d2b",
    "numbers": [
        {
            "generated_at": "2018-08-24T15:58:08.689170+00:00",
            "number": "44738010176"
        },
      . . .
      ]
}

```
___
#### GET /api/batch/unreserve/<batch_id>/<used_for>/<_number>
##### Effect
* unregisters single number from the batch
* remove numbers from modem.INSTANCE_PORTS
* db
  * store numbers in **used** table
  * store closed_at in **batches** table
* when last number is recieved set modem.STATE to **TURNED ON** 
##### Returns 
* 200 Success
```json
 [{
   "batch_id": "217ffc0e93f24980b05520a1f57d1d2b",
   "number":"44738010176",
    "used":"nike"
 }]
```
* 401 Unauthorised
 ```json
{“error” :”batch_not_yours”} 
```
* 403 Forbidden
 ```json
{“error” :”batch_not_active”}
```
* 400 Bad request
```json
{“error” :”parameters invalid”}
```
___

#### GET /api/batch/unreserve/<batch_id>/<used_for>/
##### Effect 
* unregisters entire batch
* remove numbers from modem.INSTANCE_PORTS
* db
  * store numbers in **used** table
  * store closed_at in **batches** table
* set modem.STATE to **TURNED ON** 
##### Returns 
* 200 Success
```json
 [{
   "batch_id": "217ffc0e93f24980b05520a1f57d1d2b",
   "number":"44738010176",
    "used":"nike"
 },
 . . .
]
```
* 401 Unauthorised
 ```json
{“error” :”batch_not_yours”} 
```
* 403 Forbidden
 ```json
{“error” :”batch_not_active”}
```
* 400 Bad request
```json
{“error” :”parameters invalid”}
```
___
### SMS

#### GET /api/sms/sync/<user_id>/<batch_id>
##### Effect
* loads all messages on the sims in the modem to the database
* authoirise user to see messages for the current batch
##### Returns 
```json
{
    "state": "SYNCED"
}
```
___
#### GET /api/sms/get/<user_id>/<batch_id>
##### Effect
* retrieves **all** messages for the current batch from the database
##### Returns 
* 401 Unauthorised
 ```json
{“error” :”batch_not_yours”} 
```
* 200 Success
```json
[
       {
        "id": 1, 
        "phone_number": "44757881503", 
        "recieved_at": "2018-08-23 01:34:05+00:00", 
        "_from": "Rate Advice",
        "content": "ing info free call +447782333330", 
        "batchid": "138b9716c9604182a1b7bd4cf58b044e"
      } , 
      { 
        "id": 2,
        "phone_number": "44757881503",
        "recieved_at": "2018-07-24 01:21:06+00:00",
        "_from": "Rate Advice",
        "content": "ing info free call +447782333330", 
        "batchid": "138b9716c9604182a1b7bd4cf58b044e"
      }
      . . .
]
```
___
#### GET /api/sms/get/<user_id>/<batch_id>/<_number>
##### Effect
* retrieves messages for a **specific number** in the current batch from the database
##### Returns 
* 401 Unauthorised
 ```json
{“error” :”batch_not_yours”} 
```
* 200 Success
```json
[ { 
        "id": 2,
        "phone_number": "44748259326",
        "recieved_at": "2018-07-24 01:21:06+00:00",
        "_from": "Rate Advice",
        "content": "ing info free call +447782333330", 
        "batchid": "138b9716c9604182a1b7bd4cf58b044e"
 } ]
```
___
#### GET /api/sms/get/<_userid>/<batch_id>/<_from>
##### Effect
* retrieves messages from a **specific sender**  recieved by numbers the current batch from the database
##### Returns 
* 401 Unauthorised
 ```json
{“error” :”batch_not_yours”} 
```
* 200 Success
```json
[ { 
        "id": 2,
        "phone_number": "44748259326",
        "recieved_at": "2018-07-24 01:21:06+00:00",
        "_from": "Rate Advice",
        "content": "ing info free call +447782333330", 
        "batchid": "138b9716c9604182a1b7bd4cf58b044e"
 } ]
```
* 400 Bad request
```json
{“error” :”parameters invalid”}
```
___

## Admin Methods 
### Batch 
#### GET /admin/batch/load/<_provider>
#### GET /admin/batch/state
#### GET /admin/batch/cancel
#### GET /admin/batch/close/<batch_id>

### Database
#### GET /admin/all/numbers
#### GET /admin/all/batches
#### GET /admin/all/messages
#### GET /admin/all/used

### Numbers
#### GET /admin/numbers/<_number>
